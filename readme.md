# radiobot

[@radio_pl_bot](https://telegram.me/radio_pl_bot)  - [telegram](https://telegram.org/) bot.

# Available commands:
Command		| Repsonce
------------|---------------------------------------
/start		| help information
/mc			| current playlist on [monte carlo](http://montecarlo.ru/) (105.9 FM, SPb)


# how to run
1. clone
	* ssh ```git clone git@gitlab.com:nik_b/radiobot.git```
	* https ```git clone https://gitlab.com/nik_b/radiobot.git```
1. run

	``` sh
	cd radiobot
	pip install -r requirements.txt
	python radiobot.py <telegram access token>
	```
