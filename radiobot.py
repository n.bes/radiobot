import sys
import telepot
import time
import urllib
import xml.etree.ElementTree as ET

def getHtml(url):
	responce = urllib.urlopen(url)
	return responce.read()

def getPlayListOnMonteCarlo():
	#endpoint. start from: http://montecarlospb.fm
	url = "http://rrspb.ru/monte/broad.xml"
	data = getHtml(url)

	tree = ET.fromstring(data)

	tracklist = {}
	if tree[0][0].text:
		tracklist["Prev"] = tree[0][0].text
	if tree[1][0].text:
		tracklist["Current"] = tree[1][0].text
	if tree[2][0].text:
		tracklist["Next"] = tree[2][0].text

	return tracklist

def onMsg(msg):
	content_type, chat_type, chat_id = telepot.glance(msg)

	if content_type == 'text':
		clearMsg = msg['text'].strip().lower()
		
		if clearMsg == "/start":
			message = "/mc - return current playlist on monte carlo (105.9 FM, SPb)"
			bot.sendMessage(chat_id, message)

		if clearMsg == "/mc":
			try:
				message = ""
				for key, value in getPlayListOnMonteCarlo().items():
					message += "{0}: {1}\n".format(key, value)

				message = message.strip()
				if message:
					bot.sendMessage(chat_id, message)
				else:
					bot.sendMessage(chat_id, "no info")

			except Exception as e:
				exceptionText = "Error: {0}".format(str(e))
				bot.sendMessage(chat_id, exceptionText)

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print ("usage: {0} <telegram access token>".format(sys.argv[0]))
		sys.exit(1)
	token = sys.argv[1]

	bot = telepot.Bot(token)
	bot.message_loop(onMsg)

	print ("Listening ...")
	while 1:
		time.sleep(1)
